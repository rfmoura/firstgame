﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaScript : MonoBehaviour
{
    public float velocity, jumpForce;
    private Rigidbody2D body;
    private BoxCollider2D boxCollider;
    private SpriteRenderer  spriteRenderer;
    [SerializeField] private LayerMask  plataformLayer;
    private Animator anime;

    // Start is called before the first frame update
    void Start()
    {
        this.body = transform.GetComponent<Rigidbody2D>();
        this.boxCollider = transform.GetComponent<BoxCollider2D>();
        this.spriteRenderer = transform.GetComponent<SpriteRenderer>();
        this.anime = GetComponent<Animator>();
        this.anime.enabled = false;
        Debug.Log("Anime was a Mistake");
    }

    // Update is called once per frame
    void Update()
    {
        float inputKeyHorizontal = Input.GetAxis("Horizontal");
        float x = this.transform.position.x;
        float y = this.transform.position.y;

        if(inputKeyHorizontal > 0)
        {
            this.body.AddForce(new Vector2(velocity, 0));
            this.spriteRenderer.flipX = false;
            this.anime.enabled = true;
        }
        else if (inputKeyHorizontal < 0)
        {
            this.body.AddForce(new Vector2(-velocity, 0));
            this.spriteRenderer.flipX = true;
            this.anime.enabled = true;
        }
        else
        {
            this.body.MovePosition(new Vector2(x, y));
            this.anime.enabled = false;
        }

        if(noChao() && Input.GetButton("Jump")) 
        {
            
            body.velocity = Vector2.up * jumpForce;
        }
    }
    private bool noChao()
    {
        RaycastHit2D raycastHit2D = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size, 0f, Vector2.down, .1f, plataformLayer);
        return raycastHit2D.collider != null;
    }
}
